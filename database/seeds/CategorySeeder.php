<?php

    use Illuminate\Database\Seeder;

    class CategorySeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $categories = ['Small appliances', 'Dishwashers'];

            foreach ($categories as $category) {
                DB::table('categories')->insert([
                    'name' => $category
                ]);
            }

        }
    }
