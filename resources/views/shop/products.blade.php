@extends('shop.layouts.base')

@section('js')
    @parent
    <script src="{{ URL::asset('static/js/angular.min.js') }}"></script>
@endsection

@section('js-bottom')
    @parent
    <script src="{{ URL::asset('static/js/products/angular/mfaApp.js') }}"></script>
    <script src="{{ URL::asset('static/js/products/angular/mfaConfig.js') }}"></script>
    <script src="{{ URL::asset('static/js/products/angular/services/httpServices.js') }}"></script>
    <script src="{{ URL::asset('static/js/products/angular/controllers/productsController.js') }}"></script>
@endsection

@section('content')
    <div ng-app="mfaApp" ng-controller="productsController as pc" ng-init="pc.onReady('{{ Request::url() }}',{{  @Auth::user()->id }},'{{ csrf_token() }}')">
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                    <li>
                        <a href="{{ url('/') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a>
                    </li>
                    <li class="active">Products</li>
                </ol>
            </div>
        </div>
        <!-- //breadcrumbs -->
        <!--- products --->
        <div class="products">
            <div class="container">
                <div class="col-md-4 products-left">
                    <div class="categories">
                        <h2>Categories</h2>
                        <ul class="cate">
                            @foreach($categories as $category)
                                <li>
                                    <a href="{{ url('products/categories/' . $category->id) }}">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>{{ $category->name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 products-right">
                    <div class="products-right-grid">
                        <div class="products-right-grids">
                            <div class="sorting">
                                <select id="country" class="frm-field required sect" ng-change="pc.changeOrder()" ng-model="pc.order">
                                    <option value="" @if($order==='id') selected="selected" @endif>
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>Default sorting
                                    </option>
                                    <option value="name" @if($order==='name') selected="selected" @endif>
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>Sort by tittle
                                    </option>
                                    <option value="price" @if($order==='price') selected="selected" @endif>
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>Sort by price
                                    </option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    @foreach($products as $index=>$product)
                        @if(($index++)%3===0)
                            <div class="agile_top_brands_grids">
                                @endif
                                <div class="col-md-4 top_brand_left">
                                    <div class="hover14 column">
                                        <div class="agile_top_brand_left_grid">
                                            <div class="agile_top_brand_left_grid_pos">
                                            </div>
                                            <div class="agile_top_brand_left_grid1">
                                                <figure>
                                                    <div class="snipcart-item block">
                                                        <div class="snipcart-thumb">
                                                            <a href="#">
                                                                <img class="img-responsive" src="{{ URL::asset('imgs/products/' . $product->image) }}"/>
                                                            </a>
                                                            <p>{{ $product->name }}</p>
                                                            <h4>{{ $product->price }}€
                                                                <span>{{ number_format((float)$product->price*1.10, 2, '.', '') }}€</span>
                                                            </h4>
                                                        </div>
                                                        <div class="snipcart-details top_brand_home_details">
                                                            <form action="#" method="post">
                                                                <fieldset>
                                                                    <input type="hidden" name="cmd" value="_cart">
                                                                    <input type="hidden" name="add" value="1">
                                                                    <input type="hidden" name="business" value="{{ $product->id }}">
                                                                    <input type="hidden" name="item_name" value="{{ $product->name }}">
                                                                    <input type="hidden" name="amount" value="{{ $product->price }}">
                                                                    <input type="hidden" name="discount_amount" value="0.00">
                                                                    <input type="hidden" name="currency_code" value="EUR">
                                                                    <input type="hidden" name="return" value=" ">
                                                                    <input type="hidden" name="cancel_return" value=" ">
                                                                    <input type="submit" name="submit" value="Add to cart" class="button">
                                                                </fieldset>
                                                            </form>
                                                        </div>
                                                        @if(Auth::check())
                                                            <div class="snipcart-details top_brand_home_details">
                                                                <button value="Add to wishlist" class="btn btn-info" ng-click="pc.addProductToWhishlist({{ $product->id }})">Add to wishlist</button>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                @if(($index)%3===0)
                                    <div class="clearfix"></div>
                            </div>
                        @endif
                    @endforeach
                    
                    <div class="col-md-12 text-center">
                        {!! $products->appends(\Illuminate\Support\Facades\Input::except('page'))->render() !!}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <br>
    </div>
@endsection
