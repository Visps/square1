@extends('shop.layouts.base')

@section('css')
    @parent
    <link href="{{ URL::asset('static/css/jssocials.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ URL::asset('static/css/jssocials-theme-flat.css') }}" rel="stylesheet" type="text/css" media="all"/>
@endsection

@section('js')
    @parent
    <script src="{{ URL::asset('static/js/angular.min.js') }}"></script>
    <script src="{{ URL::asset('static/js/jssocials.min.js') }}"></script>
@endsection

@section('js-bottom')
    @parent
    <script>
        $("#share").jsSocials({
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
        });
    </script>
    <script src="{{ URL::asset('static/js/products/angular/mfaApp.js') }}"></script>
    <script src="{{ URL::asset('static/js/products/angular/mfaConfig.js') }}"></script>
    <script src="{{ URL::asset('static/js/products/angular/services/httpServices.js') }}"></script>
    <script src="{{ URL::asset('static/js/products/angular/controllers/productsController.js') }}"></script>
@endsection

@section('content')
    <div ng-app="mfaApp" ng-controller="productsController as pc" ng-init="pc.onReady('{{ Request::url() }}',{{  @Auth::user()->id }},'{{ csrf_token() }}')"s>
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                    <li>
                        <a href="{{ url('/') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a>
                    </li>
                    <li class="active">Wishlist</li>
                </ol>
            </div>
        </div>
        <!-- //breadcrumbs -->
        <div class="container">
            <h3>{{ Auth::user()->wishlist->name }}</h3>
            <br/>
            @if(!count($products)>0)
                Your Wishlist is empty.
            @endif
            @foreach($products as $product)
                <div class="col-md-3 top_brand_left">
                    <div class="hover14 column">
                        <div class="agile_top_brand_left_grid">
                            <div class="agile_top_brand_left_grid_pos">
                            </div>
                            <div class="agile_top_brand_left_grid1">
                                <figure>
                                    <div class="snipcart-item block">
                                        <div class="snipcart-thumb">
                                            <a href="#">
                                                <img class="img-responsive" src="{{ URL::asset('imgs/products/' . $product->image) }}"/>
                                            </a>
                                            <p>{{ $product->name }}</p>
                                            <h4>{{ $product->price }}€
                                                <span>{{ number_format((float)$product->price*1.10, 2, '.', '') }}€</span>
                                            </h4>
                                        </div>
                                        <div class="snipcart-details top_brand_home_details">
                                            <form action="#" method="post">
                                                <fieldset>
                                                    <input type="hidden" name="cmd" value="_cart">
                                                    <input type="hidden" name="add" value="1">
                                                    <input type="hidden" name="business" value="{{ $product->id }}">
                                                    <input type="hidden" name="item_name" value="{{ $product->name }}">
                                                    <input type="hidden" name="amount" value="{{ $product->price }}">
                                                    <input type="hidden" name="discount_amount" value="0.00">
                                                    <input type="hidden" name="currency_code" value="EUR">
                                                    <input type="hidden" name="return" value=" ">
                                                    <input type="hidden" name="cancel_return" value=" ">
                                                    <input type="submit" name="submit" value="Add to cart" class="button">
                                                </fieldset>
                                            </form>
                                        </div>
                                        @if(Auth::check())
                                            <div class="snipcart-details top_brand_home_details">
                                                @if($editable)
                                                    <form method="POST" action="{{ url('wishlists/product/' . $product->id . '/remove') }}">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        {{ csrf_field() }}
                                                        <input type="submit" class="btn btn-danger" value="Remove to wishlist"/>
                                                    </form>
                                                @else
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <button value="Add to wishlist" class="btn btn-info" ng-click="pc.addProductToWhishlist({{ $product->id }})">Add to wishlist</button>
                                                    </div>
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-md-12">
                <br/>
                <div id="share"></div>
                <br/>
            </div>
        </div>
    </div>
@endsection
