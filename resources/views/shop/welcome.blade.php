@extends('shop.layouts.base')

@section('content')
    @include('shop.layouts.main_slider')
    
    <!-- top-brands -->
    <div class="top-brands">
        <div class="container">
            <h2>Top selling</h2>
            <div class="grid_3 grid_5">
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#expeditions" id="expeditions-tab" role="tab" data-toggle="tab" aria-controls="expeditions" aria-expanded="true">Small appliances</a>
                        </li>
                        <li role="presentation">
                            <a href="#tours" role="tab" id="tours-tab" data-toggle="tab" aria-controls="tours">Dishwashers</a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
                            <div class="agile-tp">
                                <h5>Lorem ipsum</h5>
                                <p class="w3l-ad">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                            </div>
                            <div class="agile_top_brands_grids">
                                @foreach($small_appliances as $sm)
                                    <div class="col-md-4 top_brand_left">
                                        <div class="hover14 column">
                                            <div class="agile_top_brand_left_grid">
                                                <div class="agile_top_brand_left_grid_pos">
                                                    <img src="{{ URL::asset('static/images/offer.png') }}" alt=" " class="img-responsive"/>
                                                </div>
                                                <div class="agile_top_brand_left_grid1">
                                                    <figure>
                                                        <div class="snipcart-item block">
                                                            <div class="snipcart-thumb">
                                                                <div class="col-md-12 max-height-products">
                                                                    <a href="#">
                                                                        <img class="img-responsive" src="{{ URL::asset('imgs/products/' . $sm->image) }}"/>
                                                                    </a>
                                                                </div>
                                                                <p>{{ $sm->name }}</p>
                                                                <div class="stars">
                                                                    <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                                    <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                                    <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                                    <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                                    <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                                </div>
                                                                <h4>{{ $sm->price }}€
                                                                    <span>{{ number_format((float)$sm->price*1.10, 2, '.', '') }}€</span>
                                                                </h4>
                                                            </div>
                                                            <div class="snipcart-details top_brand_home_details">
                                                                <form action="#" method="post">
                                                                    <fieldset>
                                                                        <input type="hidden" name="cmd" value="_cart"/>
                                                                        <input type="hidden" name="add" value="1"/>
                                                                        <input type="hidden" name="business" value="{{ $sm->id }}"/>
                                                                        <input type="hidden" name="item_name" value="{{ $sm->name }}"/>
                                                                        <input type="hidden" name="amount" value="{{ $sm->price }}"/>
                                                                        <input type="hidden" name="discount_amount" value="0.00"/>
                                                                        <input type="hidden" name="currency_code" value="EUR"/>
                                                                        <input type="hidden" name="return" value=" "/>
                                                                        <input type="hidden" name="cancel_return" value=" "/>
                                                                        <input type="submit" name="submit" value="Add to cart" class="button"/>
                                                                    </fieldset>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tours" aria-labelledby="tours-tab">
                            <div class="agile-tp">
                                <h5>Lorem ipsum</h5>
                                <p class="w3l-ad">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                            </div>
                            <div class="agile_top_brands_grids">
                                @foreach($dishwashes as $dishwash)
                                    <div class="col-md-4 top_brand_left">
                                        <div class="hover14 column">
                                            <div class="agile_top_brand_left_grid">
                                                <div class="agile_top_brand_left_grid_pos">
                                                    <img src="{{ URL::asset('static/images/offer.png') }}" alt=" " class="img-responsive"/>
                                                </div>
                                                <div class="agile_top_brand_left_grid1">
                                                    <figure>
                                                        <div class="snipcart-item block">
                                                            <div class="snipcart-thumb">
                                                                <a href="#">
                                                                    <img title="" alt="" src="{{ URL::asset('imgs/products/' . $dishwash->image) }}"/>
                                                                </a>
                                                                <p>{{ $dishwash->name }}</p>
                                                                <div class="stars">
                                                                    <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                                    <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                                    <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                                    <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                                    <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                                </div>
                                                                <h4>{{ $dishwash->price }}€
                                                                    <span>{{ number_format((float)$dishwash->price*1.10, 2, '.', '') }}€</span>
                                                                </h4>
                                                            </div>
                                                            <div class="snipcart-details top_brand_home_details">
                                                                <form action="#" method="post">
                                                                    <fieldset>
                                                                        <input type="hidden" name="cmd" value="_cart"/>
                                                                        <input type="hidden" name="add" value="1"/>
                                                                        <input type="hidden" name="business" value=" "/>
                                                                        <input type="hidden" name="item_name" value="Fortune Sunflower Oil"/>
                                                                        <input type="hidden" name="amount" value="35.99"/>
                                                                        <input type="hidden" name="discount_amount" value="1.00"/>
                                                                        <input type="hidden" name="currency_code" value="USD"/>
                                                                        <input type="hidden" name="return" value=" "/>
                                                                        <input type="hidden" name="cancel_return" value=" "/>
                                                                        <input type="submit" name="submit" value="Add to cart" class="button"/>
                                                                    </fieldset>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //top-brands -->
    <!--brands-->
    <div class="brands">
        <div class="container">
            <h3>Brand Store</h3>
            <div class="brands-agile">
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="brands-agile-1">
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="brands-agile-2">
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="col-md-2 w3layouts-brand">
                    <div class="brands-w3l">
                        <p><a href="#">Lorem</a></p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--//brands-->
@endsection