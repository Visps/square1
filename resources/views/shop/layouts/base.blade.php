<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
    <title>MyFavouriteAppliances</title>
    @section('meta')
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
    @show
    
    @section('css')
        <link href="{{ URL::asset('static/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all"/>
        <link href="{{ URL::asset('static/css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
        <link href="{{ URL::asset('static/css/font-awesome.css') }}" rel="stylesheet">
    @show
    
    @section('js')
        <script type="application/x-javascript"> addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
            }, false);
            function hideURLbar() {
                window.scrollTo(0, 1);
            } </script>
        
        <script src="{{ URL::asset('static/js/jquery-1.11.1.min.js') }}"></script>
        <link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="{{ URL::asset('static/js/move-top.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('static/js/easing.js') }}"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
    @show
</head>
<body>
@include('shop.layouts.header')

@include('shop.layouts.navigator')

@yield('content')



@include('shop.layouts.footer')

@include('shop.layouts.js_bottom')
@yield('js-bottom')

</body>
</html>