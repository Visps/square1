<!-- header -->
<div class="agileits_header">
    <div class="container">
        <div class="w3l_offers">
            @if(Auth::check())
                <p>¡Welcome {{ Auth::user()->getFullName() }}!</p>
            @endif
        </div>
        <div class="agile-login">
            <ul>
                @if(Auth::check())
                    <li><a href="{{ url('/wishlists/show/' . Auth::user()->wishlist->id) }}">Wishlist</a></li>
                    <li><a href="{{ url('/logout') }}">Logout</a></li>
                @else
                    <li><a href="{{ url('/register') }}">Create Account</a></li>
                    <li><a href="{{ url('/login') }}">Login</a></li>
                @endif
            </ul>
        </div>
        <div class="product_list_header">
            <form action="#" method="post" class="last">
                <input type="hidden" name="cmd" value="_cart">
                <input type="hidden" name="display" value="1">
                <button class="w3view-cart" type="submit" name="submit" value="">
                    <i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="logo_products">
    <div class="container">
        <div class="w3ls_logo_products_left1">
            <ul class="phone_email">
                <li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : (+0123) 234 567</li>
            </ul>
        </div>
        <div class="w3ls_logo_products_left">
            <h1><a href="{{ url('/') }}">MyFavouriteAppliances</a></h1>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- //header -->