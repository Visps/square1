<div class="navigation-agileits">
    <div class="container">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header nav_2">
                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ url('/') }}" class="act">Home</a></li>
                    <!-- Mega Menu -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Categories<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>Categories</h6>
                                        @foreach($categories as $category)
                                            <li>
                                                <a href="{{ url('products/categories/' . $category->id) }}">{{ $category->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lorem<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>Baby Care</h6>
                                        <li><a href="#">Baby Soap</a></li>
                                        <li><a href="#">Baby Care Accessories</a></li>
                                        <li><a href="#">Baby Oil & Shampoos</a></li>
                                        <li><a href="#">Baby Creams & Lotion</a></li>
                                        <li><a href="#"> Baby Powder</a></li>
                                        <li><a href="#">Diapers & Wipes</a></li>
                                    </ul>
                                </div>
                            
                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lorem<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>All Accessories</h6>
                                        <li><a href="#">Baby Food</a></li>
                                        <li><a href="#">Dessert Items</a></li>
                                        <li><a href="#">Biscuits</a></li>
                                        <li><a href="#">Breakfast Cereals</a></li>
                                        <li><a href="#"> Canned Food </a></li>
                                        <li><a href="#">Chocolates & Sweets</a></li>
                                    </ul>
                                </div>
                            
                            
                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lorem<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>Tea & Coeffe</h6>
                                        <li><a href="#">Green Tea</a></li>
                                        <li><a href="#">Ground Coffee</a></li>
                                        <li><a href="#">Herbal Tea</a></li>
                                        <li><a href="#">Instant Coffee</a></li>
                                        <li><a href="#"> Tea </a></li>
                                        <li><a href="#">Tea Bags</a></li>
                                    </ul>
                                </div>
                            
                            </div>
                        </ul>
                    </li>
                    <li><a href="#">Lorem</a></li>
                    <li><a href="#">Lorem</a></li>
                    <li><a href="#">Lorem</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>