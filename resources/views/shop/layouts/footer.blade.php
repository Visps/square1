<div class="footer">
    <div class="container">
        <div class="w3_footer_grids">
            <div class="col-md-3 w3_footer_grid">
                <h3>Contact</h3>
                
                <ul class="address">
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Calle Roderic d'Osona 4, 19
                        <span>Valencia</span></li>
                    <li>
                        <i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:adrian20es@gmail.com">adrian20es@gmail.com</a>
                    </li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+34 678 85 87 22</li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>Information</h3>
                <ul class="info">
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">About Us</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">Contact Us</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">Short Codes</a>
                    </li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">FAQ's</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">Special Products</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>Category</h3>
                <ul class="info">
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">Groceries</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">Household</a></li>
                    <li>
                        <i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">Personal Care</a>
                    </li>
                    <li>
                        <i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">Packaged Foods</a>
                    </li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">Beverages</a></li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>Profile</h3>
                <ul class="info">
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">Store</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="#">My Cart</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{ url('/login') }}">Login</a></li>
                    <li>
                        <i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{ url('/register') }}">Create Account</a>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    
    <div class="footer-copy">
        
        <div class="container">
            <p>© 2017 MyFavouriteAppliances. All rights reserved | Design by
                <a href="http://w3layouts.com/">W3layouts</a></p>
        </div>
    </div>

</div>
<div class="footer-botm">
    <div class="container">
        <div class="w3layouts-foot">
            <ul>
                <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                <li><a href="#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <div class="payment-w3ls">
            <img src="{{ URl::asset('static/images/card.png') }}" alt=" " class="img-responsive">
        </div>
        <div class="clearfix"></div>
    </div>
</div>