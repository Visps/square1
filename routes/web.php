<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get('/', 'HomeController@index');

    // Custom routes for Auth
    // Auth::routes();
    // Authentication Routes...
    Route::get('login', 'AuthController@login')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    // Registration Routes...
    Route::get('register', 'AuthController@register')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

    Route::group(['prefix' => 'products', 'middleware' => []], function () {
        Route::get('/{product}', 'ProductController@index');
        Route::get('/categories/{category}', 'ProductsController@getProductsByCategory');
    });

    Route::group(['prefix' => 'wishlists', 'middleware' => ['auth']], function () {
        Route::get('/show/{wishlist}', 'WishlistController@index');
        Route::delete('/product/{product}/remove', 'WishlistController@deleteProduct');
        Route::post('/product/add', 'WishlistController@addProduct');
    });




