/**
 * Created by adrian on 24/04/17.
 */
(function (mfaApp) {
    function config($interpolateProvider) {
        $interpolateProvider.startSymbol('{$');
        $interpolateProvider.endSymbol('$}');

        _baseUrl = document.location.origin;
        if (_baseUrl === "http://localhost" || _baseUrl === "http://127.0.0.1") {
            _baseUrl = _baseUrl + "/square1/public";
        }
    }

    mfaApp.config(config);
}(angular.module("mfaApp")));