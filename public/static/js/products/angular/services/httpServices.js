/**
 * Created by adrian on 24/04/17.
 */

(function (mfaApp) {
    httpService.$inject = ['$http'];
    function httpService($http) {
        return {
            // Get
            get: function (path) {
                return $http.get(path);
            },

            // Save
            save: function (path, csrf, data) {
                return $http({
                    method: 'POST',
                    url: path,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'X-CSRFToken': csrf
                    },
                    data: $.param(data)
                });
            },

            // Update
            update: function (path, csrf, data) {
                return $http({
                    method: 'PUT',
                    url: path,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'X-CSRFToken': csrf
                    },
                    data: $.param(data)
                });
            },

            // Destroy
            destroy: function (path) {
                return $http.delete(path);
            }
        }
    }

    mfaApp.factory("httpService", httpService);
})(angular.module('mfaApp'));