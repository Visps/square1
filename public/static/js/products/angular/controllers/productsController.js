/**
 * Created by adrian on 24/04/17.
 */

(function (mfaApp) {
    productsController.$inject = ['httpService'];
    function productsController(httpService) {
        var vm = this;
        window.r = vm;

        vm.onReady = function (_current_url, _user_id, _csrf) {
            vm.current_url = _current_url;
            vm.user_id = _user_id;
            vm.csrf = _csrf;
        }

        vm.changeOrder = function () {
            var _param = '';
            if (vm.order != '') {
                _param = '?order=' + vm.order;
            }
            window.location.href = vm.current_url + _param;
        }

        vm.addProductToWhishlist = function (_product_id) {
            var _data = {
                'user_id': vm.user_id,
                'product_id': _product_id
            };

            httpService.save(_baseUrl + "/wishlists/product/add", vm.csrf, _data).then(
                function (data) {
                    alert("Product added a your Wishlist!")
                },
                function (response) {
                    alert("Error, try again later...");
                }
            );
        }

    }

    mfaApp.controller("productsController", productsController);
})
(angular.module('mfaApp'));