#Appliances Delivered

Run next commands:

Install Laravel dependencies
* composer update

Edit your .env
* SESSION_DRIVER=database
* QUEUE_DRIVER=database

Insert in crontab 
* (* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1) OPTIONAL  for sync.

Migration and seeder
* php artisan migrate --seed (Seeders for categories)

Custom command to create jobs
* php artisan appliances:populate_db

Execute jobs with delay (Delay is optional, for test withou sleep)
* php artisan queue:listen --sleep=3

Execute server
* php artisan serve (http://127.0.0.1:8000)