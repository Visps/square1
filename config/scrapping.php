<?php

    return [

        /*
        |--------------------------------------------------------------------------
        | Default Urls for Scrapping products in AppliancesDelivered
        |--------------------------------------------------------------------------
        |
        | Only supports urls from www.appliancesdelivered.ie
        |
        */

        'urls' => [
            \MyFavouriteAppliances\Models\Category::SMALL_APPLIANCES => 'https://www.appliancesdelivered.ie/small-appliances',
            \MyFavouriteAppliances\Models\Category::DISHWASHERS => 'https://www.appliancesdelivered.ie/dishwashers'
        ],
    ];
