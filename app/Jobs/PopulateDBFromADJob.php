<?php

    namespace MyFavouriteAppliances\Jobs;

    use Illuminate\Bus\Queueable;
    use Illuminate\Contracts\Queue\ShouldQueue;
    use Illuminate\Foundation\Bus\Dispatchable;
    use Illuminate\Queue\InteractsWithQueue;
    use Illuminate\Queue\SerializesModels;
    use MyFavouriteAppliances\Libraries\Scrapping;
    use MyFavouriteAppliances\Models\Category;
    use MyFavouriteAppliances\Models\Product;

    class PopulateDBFromADJob implements ShouldQueue
    {
        use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

        protected $url_base;
        protected $category_id;

        /**
         * Create a new job instance.
         *
         * @param $url_base
         * @param $category_id
         */
        public function __construct($url_base, $category_id)
        {
            $this->url_base = $url_base;
            $this->category_id = $category_id;
        }

        /**
         * Execute the job.
         *
         * @return void
         */
        public function handle()
        {
            $scrapping = new Scrapping();
            $products = $scrapping->scrapping($this->url_base);
            if ($products) {
                foreach ($products as $pd) {
                    // Create product
                    $product = Product::firstOrNew(['ad_id' => $pd['ad_id']]);
                    $product->deleteImage(Scrapping::IMAGES_PATH);
                    $product->fill($pd);
                    $product->save();

                    // Create relation Product-Category
                    if ($product->exists) {
                        $category = Category::where('id', $this->category_id)->first();
                        if ($category->exists) {
                            $product->categories()->save($category);
                        }
                    }
                }
            }
        }
    }
