<?php
    /**
     * Created by PhpStorm.
     * User: adrian
     * Date: 22/04/17
     * Time: 15:30
     */

    namespace MyFavouriteAppliances\Libraries;

    use Goutte\Client;
    use Symfony\Component\DomCrawler\Crawler;

    /**
     * Class Scrapping
     * This class only work for AppliancesDelivered web.
     *
     * @package MyFavouriteAppliances\Libraries
     */
    class Scrapping
    {

        private $client;
        private $products = [];

        const URL_VARIABLE_PAGINATION = '?page=';
        const IMAGES_PATH = 'imgs/products/';

        /**
         * Scrapping constructor.
         */
        public function __construct()
        {
            $this->client = new Client();
        }

        /**
         * Job generator for scrapping
         *
         * @param $url_base
         *
         * @return array
         */
        public function createScrappingByUrl($url_base)
        {
            $total_pages = $this->getNumberPagesByUrl($url_base);
            $urls = $this->createUrlsPaginationByUrlAndNumber($url_base, $total_pages);

            return $urls;
        }

        /**
         * Get total pages for scrapping
         *
         * @param $url_base
         *
         * @return mixed
         */
        private function getNumberPagesByUrl($url_base)
        {
            $crawler = $this->client->request('GET', $url_base);
            $last_url = $crawler->filter('.result-list-pagination a')->last()->attr('href');
            $limit = explode('=', $last_url);

            return end($limit);
        }

        /**
         * Create urls pagination for scrapping
         *
         * @param $url
         * @param $number
         *
         * @return array
         */
        private function createUrlsPaginationByUrlAndNumber($url, $number)
        {
            $urls = [];
            for ($count = 1; $count <= $number; $count++) {
                $urls[] = $url . self::URL_VARIABLE_PAGINATION . $count;
            }

            return $urls;
        }

        /**
         * Scrapping products from web
         *
         * @param $url
         *
         * @return array
         */
        public function scrapping($url)
        {
            try {
                $crawler = $this->client->request('GET', $url);
                $crawler->filter('.search-results-product')->each(function (Crawler $node, $number_loop) {
                    $product = [];
                    $product['image'] = $this->downloadImageByUrl($node->filter('.product-image img')->attr('src'));
                    $product['name'] = $node->filter('.product-description h4')->text();
                    $price = $node->filter('.section-title')->text();
                    $product['price'] = str_replace('€', '', $price);
                    $action = $node->filter('form')->attr('action');
                    $product['ad_id'] = $this->getProductIdByURL($action);

                    $this->products[] = $product;
                });
            } catch (\InvalidArgumentException $e) {
                //Nothing
            } catch (\RuntimeException $e) {
                //Nothing
            }

            return $this->products;
        }

        /**
         * Download image from a URL
         *
         * @param $url
         *
         * @return bool|int
         * @internal param string $path
         *
         */
        private function downloadImageByUrl($url)
        {
            $image = file_get_contents($url);
            $filename = $this->setFilenameByImage($image, $url);
            $path = public_path(self::IMAGES_PATH . $filename);
            $insert = file_put_contents($path, $image);
            if ($insert) {
                return $filename;
            }

            return $url;
        }

        /**
         * Obtain a filename
         *
         * @param $image
         * @param $url
         *
         * @return string
         */
        private function setFilenameByImage($image, $url)
        {
            //Not have extensions
            //            $image_exploded = explode('.', $url);
            //            $extension = end($image_exploded);
            $filename = md5(uniqid(mt_rand(0, 99), TRUE) . $image);// . '.' . $extension;

            return $filename;
        }

        /**
         * Get product ID by URL buy
         *
         * @param $url
         *
         * @return mixed
         */
        private function getProductIdByURL($url)
        {
            $exploded_url = explode('/', $url);

            return end($exploded_url);
        }
    }