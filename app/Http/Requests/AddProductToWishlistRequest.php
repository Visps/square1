<?php

    namespace MyFavouriteAppliances\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;

    class AddProductToWishlistRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return TRUE;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'user_id' => 'required|integer',
                'product_id' => 'required|integer'
            ];
        }
    }
