<?php

    namespace MyFavouriteAppliances\Http\Controllers;

    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use MyFavouriteAppliances\Http\Requests\AddProductToWishlistRequest;
    use MyFavouriteAppliances\Models\Product;
    use MyFavouriteAppliances\Models\Wishlist;

    class WishlistController extends MainController
    {
        /**
         * Display a listing of the resource.
         *
         * @param Wishlist $wishlist
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Wishlist $wishlist)
        {
            $wishlist_user = Auth::user()->wishlist()->first();
            if ($wishlist_user->id == $wishlist->id) {
                $this->data['editable'] = TRUE;
                $this->data['products'] = $wishlist_user->products()->get();
            } else {
                $this->data['editable'] = FALSE;
                $this->data['products'] = $wishlist->products()->get();
            }

            return view('shop.wishlists', $this->data);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param Request $request
         * @param Product $product
         *
         * @return \Illuminate\Http\Response
         * @internal param int $id
         *
         */
        public function deleteProduct(Request $request, Product $product)
        {
            $wishlist = Wishlist::where('user_id', Auth::user()->id)->first();
            $wishlist->products()->detach($product);

            return redirect('/wishlists/show/' . $wishlist->id);
        }


        /**
         * Associate new product in a wishlist
         *
         * @param Request|AddProductToWishlistRequest $request
         *
         * @return \Illuminate\Http\Response
         * @internal param Product $product
         *
         */
        public function addProduct(AddProductToWishlistRequest $request)
        {
            $status = 'KO';
            $data = 'KO';
            $http_response_header = 400;

            try {
                $current_user = Auth::user();
                if ($current_user->id == $request->user_id) {
                    $product = Product::where('id', $request->product_id)->firstOrFail();
                    $wishlist = Wishlist::where('user_id', $current_user->id)->firstOrFail();
                    if (!$wishlist->products->contains($product->id)) {
                        $wishlist->products()->attach($product->id);
                        $status = 'OK';
                        $data = 'OK';
                        $http_response_header = 201;
                    }
                }
            } catch (ModelNotFoundException $e) {
                //Nothing
            }

            return response()->json(['status' => $status, 'data' => $data], $http_response_header);

        }

    }
