<?php

    namespace MyFavouriteAppliances\Http\Controllers;

    class AuthController extends MainController
    {
        public function login()
        {
            return view('shop.login', $this->data);
        }

        public function register()
        {
            return view('shop.register', $this->data);
        }
    }
