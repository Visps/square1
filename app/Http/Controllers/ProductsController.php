<?php

    namespace MyFavouriteAppliances\Http\Controllers;

    use Illuminate\Http\Request;
    use MyFavouriteAppliances\Models\Category;
    use MyFavouriteAppliances\Models\Product;

    class ProductsController extends MainController
    {
        const MAX_PRODUCTS_SHOW = 12;

        /**
         * Display a listing of the resource.
         *
         * @param Product $product
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Product $product)
        {
            $this->data['product'] = $product;

            return view('shop.products', $this->data);
        }

        /**
         * Show the products by category
         *
         * @param Category $category
         * @param Request $request
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         * @internal param string $order
         *
         */
        public function getProductsByCategory(Category $category, Request $request)
        {
            if ($request->order != 'price' && $request->order != 'name') {
                $order = 'id';
            } else {
                $order = $request->order;
            }

            $this->data['order'] = $order;
            $this->data['category'] = $category;
            $this->data['products'] = Category::where('id', $category->id)->first()->products()->orderBy($order, 'desc')->paginate(self::MAX_PRODUCTS_SHOW);
            $this->data['links'] = '';

            return view('shop.products', $this->data);
        }


        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

    }
