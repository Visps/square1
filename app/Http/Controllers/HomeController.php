<?php

    namespace MyFavouriteAppliances\Http\Controllers;

    use MyFavouriteAppliances\Models\Category;

    class HomeController extends MainController
    {
        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $this->data['small_appliances'] = Category::where('id', Category::SMALL_APPLIANCES)->first()->products()->take(3)->get();
            $this->data['dishwashes'] = Category::where('id', Category::DISHWASHERS)->first()->products()->take(3)->get();

            return view('shop.welcome', $this->data);
        }
    }
