<?php

    namespace MyFavouriteAppliances\Http\Controllers;

    use Illuminate\Support\Facades\Auth;
    use MyFavouriteAppliances\Models\Category;

    class MainController extends Controller
    {

        protected $data = [];

        /**
         * MainController constructor.
         */
        public function __construct()
        {
            if (Auth::check()) {
                $this->data['current_user'] = Auth::user();
            }

            $this->data['categories'] = Category::all();
        }
    }
