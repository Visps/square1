<?php

    namespace MyFavouriteAppliances\Console\Commands;

    use Illuminate\Console\Command;
    use MyFavouriteAppliances\Jobs\PopulateDBFromADJob;
    use MyFavouriteAppliances\Libraries\Scrapping;

    class PopulateDB extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'appliances:populate_db';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Populate DB from web';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $config = config('scrapping.urls');
            $scrapping = new Scrapping();
            foreach ($config as $category => $url) {
                $paginated_urls = $scrapping->createScrappingByUrl($url);
                if (count($paginated_urls) > 0) {
                    foreach ($paginated_urls as $final_url) {
                        $job = new PopulateDBFromADJob($final_url, $category);//->delay(Carbon::now()->addMinutes(5));
                        dispatch($job);
                    }
                }
            }
        }
    }
