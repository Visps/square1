<?php

    namespace MyFavouriteAppliances\Console;

    use Illuminate\Console\Scheduling\Schedule;
    use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
    use MyFavouriteAppliances\Console\Commands\PopulateDB;

    class Kernel extends ConsoleKernel
    {
        /**
         * The Artisan commands provided by your application.
         *
         * @var array
         */
        protected $commands = [
            PopulateDB::class
        ];

        /**
         * Define the application's command schedule.
         *
         * @param  \Illuminate\Console\Scheduling\Schedule $schedule
         *
         * @return void
         */
        protected function schedule(Schedule $schedule)
        {
            $schedule->command('appliances:populate_db')->weekly()->wednesdays()->at('03:00');
        }

        /**
         * Register the Closure based commands for the application.
         *
         * @return void
         */
        protected function commands()
        {
            require base_path('routes/console.php');
        }
    }
