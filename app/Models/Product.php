<?php

    namespace MyFavouriteAppliances\Models;

    use Illuminate\Database\Eloquent\Model;

    class Product extends Model
    {
        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'products';

        /**
         * The attributes for timing.
         *
         * @var boolean
         */
        public $timestamps = TRUE;

        /**
         * The attributes that should be casted to native types.
         *
         * @var array
         */
        protected $casts = [
            'name' => 'string',
            'price' => 'float',
            'image' => 'string',
            'ad_id' => 'integer'
        ];
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name',
            'price',
            'image',
            'ad_id'
        ];

        /**
         * Get the wishlist associated with the product.
         */
        public function wishlists()
        {
            return $this->belongsToMany(Wishlist::class, 'wishlists_products');
        }

        /**
         * Get the categories associated with the product.
         */
        public function categories()
        {
            return $this->belongsToMany(Category::class, 'products_categories')->withTimestamps();
        }

        /**
         * Delete image from local disk
         *
         * @param $path
         *
         * @return bool
         */
        public function deleteImage($path)
        {
            $path_file = public_path($path . $this->image);
            if (file_exists($path_file) && $this->image != '') {
                return unlink($path_file);
            }
        }
    }
