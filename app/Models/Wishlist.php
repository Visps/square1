<?php

    namespace MyFavouriteAppliances\Models;

    use Illuminate\Database\Eloquent\Model;

    class Wishlist extends Model
    {
        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'wishlists';

        /**
         * The attributes for timing.
         *
         * @var boolean
         */
        public $timestamps = TRUE;

        /**
         * The attributes that should be casted to native types.
         *
         * @var array
         */
        protected $casts = [
            'name' => 'string',
            'user_id' => 'integer'
        ];
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name',
            'user_id'
        ];

        /**
         * Get the user record associated with the Wishlist.
         */
        public function user()
        {
            return $this->belongsTo(User::class);
        }

        /**
         * Get the products associated with the Wishlist.
         */
        public function products()
        {
            return $this->belongsToMany(Product::class, 'wishlists_products');
        }

    }
