<?php

    namespace MyFavouriteAppliances\Models;

    use Illuminate\Database\Eloquent\Model;

    class Category extends Model
    {

        /**
         * Constants for categories
         */
        const SMALL_APPLIANCES = 1;
        const DISHWASHERS = 2;

        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'categories';

        /**
         * The attributes for timing.
         *
         * @var boolean
         */
        public $timestamps = TRUE;

        /**
         * The attributes that should be casted to native types.
         *
         * @var array
         */
        protected $casts = [
            'name' => 'string'
        ];
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name'
        ];

        /**
         * Get the products associated with the category.
         */
        public function products()
        {
            return $this->belongsToMany(Product::class, 'products_categories')->withTimestamps();
        }
    }
