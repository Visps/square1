<?php

    namespace MyFavouriteAppliances\Models;

    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Notifications\Notifiable;

    class User extends Authenticatable
    {
        use Notifiable;

        const NAME_FIRST_WISHLIST = 'My first wishlist';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'first_name',
            'last_name',
            'email',
            'password',
        ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password',
            'remember_token',
        ];

        /**
         * Get the wishlist associated with the user.
         */
        public function wishlist()
        {
            return $this->hasOne(Wishlist::class);
        }

        /**
         * Get full name
         * @return string
         */
        public function getFullName()
        {
            return $this->first_name . ' ' . $this->last_name;
        }

        /**
         * Create a wishlist associated with this user
         */
        public function createFirstWishlist()
        {
            $wishlist = new Wishlist();
            $wishlist->name = self::NAME_FIRST_WISHLIST;
            $wishlist->user_id = $this->id;
            $wishlist->save();
        }
    }
